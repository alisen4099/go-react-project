import React, {useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {Button, Card, Row, Col} from 'react-bootstrap'
const User = ({userData, deleteSingleUser, setChangeUser}) => {
    return (
        <Card>
            <Row>
                <Col>Name:{ userData !== undefined && userData.name}</Col>
                <Col>Surname:{ userData !== undefined && userData.surname}</Col>
                <Col>Email:{ userData !== undefined && userData.email}</Col>
                <Col><Button onClick={() => deleteSingleUser(userData._id)}>Delete User</Button></Col>
                <Col><Button onClick={() => changeUser()}>Update User</Button></Col>
            </Row>
        </Card>
    )

    function changeUser(){
        setChangeUser({
            "change": true,
            "id": userData._id
        })
    }
}
export default User