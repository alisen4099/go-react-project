import React, {useState, useEffect} from 'react';
import axios from "axios";
import {Button, Form, Container, Modal } from 'react-bootstrap'
import User from './single-user.components';
const Users = () => {
    const [users, setUsers] = useState([])
    const [refreshData, setRefreshData] = useState(false)

    const [changeUser, setChangeUser] = useState({"change": false, "id": 0})

    const [addNewUser, setAddNewUser] = useState(false)
    const [newUser, setNewUser] = useState({"name": "", "surname": "", "email":""})

    //gets run at initial loadup
    useEffect(() => {
        getAllUsers();
    }, [])

    //refreshes the page
    if(refreshData){
        setRefreshData(false);
        getAllUsers();
    }

    return (
        <div>

            {/* add new user button */}
            <Container>
                <Button onClick={() => setAddNewUser(true)}>Create</Button>
            </Container>

            {/* list all current users */}
            <Container>
                {users != null && users.map((user, i) => (
                    <User userData={user} deleteSingleUser={deleteSingleUser}  setChangeUser={setChangeUser}/>
                ))}
            </Container>

            {/* popup for adding a new user */}
            <Modal show={addNewUser} onHide={() => setAddNewUser(false)} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Create</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group>
                        <Form.Label >name</Form.Label>
                        <Form.Control onChange={(event) => {newUser.name = event.target.value}}/>
                        <Form.Label>surname</Form.Label>
                        <Form.Control onChange={(event) => {newUser.surname = event.target.value}}/>
                        <Form.Label >email</Form.Label>
                        <Form.Control onChange={(event) => {newUser.email = event.target.value}}/>
                    </Form.Group>
                    <Button onClick={() => addSingleUser()}>Add</Button>
                    <Button onClick={() => setAddNewUser(false)}>Cancel</Button>
                </Modal.Body>
            </Modal>

            {/* popup for changing a user */}
            <Modal show={changeUser.change} onHide={() => setChangeUser({"change": false, "id": 0})} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Update User</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group>
                        <Form.Label >name</Form.Label>
                        <Form.Control onChange={(event) => {newUser.name = event.target.value}}/>
                        <Form.Label>surname</Form.Label>
                        <Form.Control onChange={(event) => {newUser.surname = event.target.value}}/>
                        <Form.Label >email</Form.Label>
                        <Form.Control onChange={(event) => {newUser.email = event.target.value}}/>
                    </Form.Group>
                    <Button onClick={() => changeSingleUser()}>Save</Button>
                    <Button onClick={() => setChangeUser({"change": false, "id": 0})}>Cancel</Button>
                </Modal.Body>
            </Modal>
        </div>
    );
    //changes the user
    function changeSingleUser(){
        changeUser.change = false;
        var url = "http://localhost:5050/user/update/" + changeUser.id
        axios.put(url, newUser)
            .then(response => {
                if(response.status == 200){
                    setRefreshData(true)
                }
            })
    }

    //creates a new user
    function addSingleUser(){
        setAddNewUser(false)
        var url = "http://localhost:5050/user/create"
        axios.post(url, {
            "name": newUser.name,
            "surname": newUser.surname,
            "email": newUser.email,
        }).then(response => {
            if(response.status == 200){
                setRefreshData(true)
            }
        })
    }

    //gets all the users
    function getAllUsers(){
        var url = "http://localhost:5050/users"
        axios.get(url, {
            responseType: 'json'
        }).then(response => {
            if(response.status == 200){
                setUsers(response.data)
            }
        })
    }

    //deletes a single user
    function deleteSingleUser(id){
        var url = "http://localhost:5050/user/delete/" + id
        axios.delete(url, {

        }).then(response => {
            if(response.status == 200){
                setRefreshData(true)
            }
        })
    }

}
export default Users