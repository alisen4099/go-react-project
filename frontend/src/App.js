import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';

import Users from './components/users.components';

function App() {
  return (
      <div>
        <Users />
      </div>
  );
}

export default App;