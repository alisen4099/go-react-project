package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive" // so every object id will be unique
)

type User struct {
	ID      primitive.ObjectID `bson:"_id"`
	Name    *string            `json:"name"`
	Surname *string            `json:"surname""`
	Email   *string            `json:"email"`
}
