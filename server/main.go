package main

import (
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"server/routes"
)

func main() {

	port := os.Getenv("PORT")

	if port == "" {
		port = "8000"
	}

	router := gin.New()
	router.Use(gin.Logger())

	router.Use(cors.Default())

	// these are the endpoints
	//C
	router.POST("/user/create", routes.CreateUser)
	//R
	router.GET("/user/:id", routes.GetUserById)
	router.GET("/users", routes.GetUsers)
	//U
	router.PUT("/user/update/:id", routes.UpdateUser)
	//D
	router.DELETE("/user/delete/:id", routes.DeleteUser)

	//this runs the server and allows it to listen to requests.
	router.Run(":" + port)
}
